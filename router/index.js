import express  from 'express';
import  json  from 'body-parser';
import { render } from 'ejs';

export const router = express.Router();

// Declarar primer ruta por omisión
// Ruta por omisión
router.get('/', (req, res) => {
    res.render('index', { titulo: "Compañia de Luz", nombre: "Jesus Esteban Morales Niebla" });
});

// Ruta para el Pago de Recibo
router.get('/recibo', (req, res) => {
    const params = {
        numRecibo: req.query.numRecibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        servicio: req.query.servicio,
        kwConsumidos: req.query.kwConsumidos,
        isPost: false,
    };
    res.render("recibo", params);    
});

// Ruta para procesar la tabla de Pago de Recibo
router.post('/recibo', (req, res) => {
    const precios = [1.08, 2.5, 3.0];

    const { numRecibo, nombre, domicilio, servicio, kWCon } = req.body;
    const costKw = precios[servicio * 1];
    const tipoServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial'
    const subtotal = costKw * kWCon;

    // Calcular costo total de los kilowatts consumidos

    // Calcular el descuento
    let descuento = 0;
    if (kWCon <= 1000) {
        descuento = 0.1;
    } else if (kWCon > 1000 && kWCon <= 10000) {
        descuento = 0.2;
    } else {
        descuento = 0.5;
    }



    // Aplicar el descuento al subtotal
    const descApply = subtotal * descuento;
    

    // Calcular el impuesto
    const impuesto = 0.16 * subtotal;

    // Calcular el total a pagar
    const total = subtotal + impuesto;

    // Aplicar el descuento de total a pagar
    const subtotalDesc = subtotal + impuesto - descApply;

    // Actualizar el objeto 'resultado'
    const params = {
        numRecibo,
        nombre,
        domicilio,
        servicio: tipoServicio,
        kWCon,
        costKw,
        subtotal,
        descuento: descApply, // Actualizado para reflejar el descuento aplicado
        subtotalDesc,
        impuesto,
        total,
        isPost: true,
    };
    res.render("recibo", params);
});

export default {router}